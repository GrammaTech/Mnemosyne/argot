(defpackage :argot/readtable
  (:documentation "A case-inverting readtable with support for curry-compose-reader-macros.")
  (:use :gt/full)
  (:export :argot-readtable))
(in-package :argot/readtable)

(defreadtable argot-readtable
  (:fuse :standard :curry-compose-reader-macros)
  (:case :invert))
