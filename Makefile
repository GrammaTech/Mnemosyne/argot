# Set personal or machine-local flags in a file named local.mk
ifneq ("$(wildcard local.mk)","")
include local.mk
endif

all: argot

PACKAGE_NAME = argot

LISP_DEPS = $(wildcard *.lisp)

include .cl-make/cl.mk

.qlfile.external:
	touch $@
