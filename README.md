# Argot LSP

This repository contains the Lisp implementation of Argot, a superset
of [LSP][] that supports explicitly represented (i) existential
quantifiers such as examples and *tests*, (ii) universal quantifiers
such as contracts and *types*, and (iii) natural language
documentation and *prose*.

[Documentation for Argot][arch] is available as part of the
documentation for the [Mnemosyne][] system, of which it is a part.

(If you are looking for lenses for translation between programming
languages, see [Fresnel][]).

[LSP]: https://microsoft.github.io/language-server-protocol/
[arch]: https://grammatech.gitlab.io/Mnemosyne/docs/architecture/
[Mnemosyne]: https://grammatech.gitlab.io/Mnemosyne/docs/
[Fresnel]: https://gitlab.com/GrammaTech/Mnemosyne/fresnel
