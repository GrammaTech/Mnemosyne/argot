(defsystem "argot"
  :name "Argot"
  :author "GrammaTech"
  :license "MIT"
  :description "LSP superset for representing program kinds."
  :depends-on (:argot/argot)
  :class :package-inferred-system
  :in-order-to ((test-op (load-op "argot/test")))
  :perform (test-op (o c) (symbol-call :argot/test '#:run-batch)))

(register-system-packages "lsp-server" '(:lsp-server/protocol))
(register-system-packages "lsp-server" '(:lsp-server/protocol-util))
(register-system-packages "lsp-server" '(:lsp-server/lsp-server))
(register-system-packages "lsp-server" '(:lsp-server/logger))
