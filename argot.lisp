(uiop:define-package :argot/argot
  (:use :gt/full
        :lsp-server/protocol
        :argot/readtable)
  (:nicknames :argot)
  (:shadowing-import-from :trivial-types :proper-list)
  (:export
    :test-path :|TestPath| :test-name :name
    :test-status-message
    :test-case-result
    :unit-test-case-result
    :binary-test-case-result
    :test-status
    :passed
    :skipped
    :unknown
    :failed
    :errored
    :test-case
    :unit-test-case
    :binary-test-case
    :test-suite
    :test-framework
    :test-framework-case-class
    :test-framework-suite-class
    :test-disabled-p
    :test-file
    :test-case-conditions
    :test-target
    :output
    :stdout
    :target
    :arguments
    :expected
    :environment
    :test-cases
    :setup
    :fixture-unit-test-case
    :result
    :successful-p
    :test-case-verification-result
    :teardown
    :verification-results
    :stderr
    :exit-code
    :fatal-test-condition
    :non-fatal-test-condition
    :comparator
    :example-call
    :test-case-examples
    :data
    :subscripts
    "+argot/window/showPrompt+"
    "+argot/window/goto+"
    "+argot/textDocument/content+"
    :+argot/workspace/files+
    "+argot/didAnnotate+"
    "+argot/getAnnotations+"
    "+argot/forwardClientRequest+"
    "+inputBoxProvider+"
    "+filesProvider+"
    "+contentProvider+"
    :argot-method-option
    :argot-option-methods
    :argot-option?
    :check-argot-option))
(in-package :argot/argot)
(in-readtable argot-readtable)

(deftype Env () 'hash-table)

;;; Constants for method names.
(defconst +argot/window/showPrompt+ "argot/window/showPrompt")
(defconst |+argot/window/goto+| "argot/window/goto")
(defconst +argot/textDocument/content+ "argot/textDocument/content")
(defconst +argot/workspace/files+ "argot/workspace/files")
(defconst +argot/didAnnotate+ "argot/didAnnotate")
(defconst +argot/getAnnotations+ "argot/getAnnotations")
(defconst +argot/forwardClientRequest+ "argot/forwardClientRequest")

;;; Constants for initialization options.
(defconst +inputBoxProvider+ "inputBoxProvider")
(defconst +filesProvider+ "filesProvider")
(defconst +contentProvider+ "contentProvider")
(defconst +gotoProvider+ "gotoProvider")

(def +argot-method-option-table+
  `((,+argot/window/showPrompt+ . ,+inputBoxProvider+)
    (,|+argot/window/goto+| . ,+gotoProvider+)
    (,+argot/textDocument/content+ . ,+contentProvider+)
    (,+argot/workspace/files+ . ,+filesProvider+)))

(-> argot-option? (string) boolean)
(defun argot-option? (string)
  "Is STRING an Argot option?"
  (not (null (find string +argot-method-option-table+
                   :key #'cdr :test #'equal))))

(defun check-argot-option (string)
  "Signal an error if STRING is not an Argot option."
  (unless (argot-option? string)
    (error "~a is not an Argot option" string)))

(-> argot-method-option (string)
  (values (or string null)
          (or null cons)))
(defun argot-method-option (method)
  "Get the option that controls Argot method METHOD."
  (assocdr method +argot-method-option-table+ :test #'equal))

(-> argot-option-methods (string) (values list &optional))
(defun argot-option-methods (option)
  "Return the methods controlled by OPTION."
  (check-argot-option option)
  (mapcar #'car
          (keep option +argot-method-option-table+
                :key #'cdr
                :test #'equal)))

(define-interface ArgotShowPromptParams (ShowMessageRequestParams)
  (defaultValue :optional t :type string)
  (multiLine :optional t :type boolean))

;;; Cf. https://github.com/sourcegraph/language-server-protocol/blob/master/extension-files.md

(define-interface ArgotFilesParams ()
  (|base|
   :type string
   :optional t
   :documentation
   "The URI of a directory to search. Can be relative to the rootPath.
   If not given, defaults to rootPath."))

(define-interface ArgotContentParams ()
  (textDocument
   :type TextDocumentIdentifier
   :documentation
   "The text document to receive the content for."))

(define-namespaced-constants ArgotKind ()
  (Code  :|code|)
  (Types :|types|)
  (Tests :|tests|)
  (Prose :|prose|))

;;; Of course it would be nicer to have different contribution flavors
;;; with their own interfaces, but that information would be lost when
;;; serializing/deserializing.

(define-interface Contribution ()
  (|source| :type string
            :documentation "The muse responsible for this contribution.")
  (|key| :type (or number string) :optional t
         :documentation "An optional sub-key for the contribution.
Within an Annotation, Contributions are addressed by source and key.
If there is no key, there can only be one Contribution from a given
muse per Annotation.")
  (|kind| :type ArgotKind)
  (|data| :type (or Data hash-table)
          :documentation "A dictionary of properties."))

(defgeneric data (contrib)
  (:method ((self Contribution))
    (slot-value self '|data|)))

(define-interface Annotation ()
  (textDocument :type TextDocumentIdentifier
                :documentation "The file to annotate.")
  (|range| :optional t :type |Range|
           :documentation "The range the annotation applies to.
An annotation without a range applies to the whole file.")
  (|contributions| :type (proper-list Contribution)))

(define-interface GetAnnotationsParams ()
  (textDocument :type TextDocumentIdentifier)
  (|range| :optional t :type |Range| :documentation "No range means to get the annotations for the whole file."))

(define-interface DidAnnotateParams ()
  (|annotations| :type (proper-list Annotation)))

(define-interface ArgotRun ()
  (|kind| :type ArgotKind)
  (textDocument :type TextDocumentIdentifier)
  (|env| :type Env))

(define-interface Data ()
  (|interface| :optional t :type string))

(defmethod initialize-instance :after ((self Data) &key)
  (setf (slot-value self '|interface|)
        (string (interface-name self))))

(defmethod convert-from-hash-table ((instance Data) (table hash-table))
  (or (when-let ((class-name (gethash "interface" table)))
        (when (stringp class-name)
          (when-let (class (name-protocol-symbol class-name :error t))
            (unless (typep instance class)
              (convert-from-hash-table class table)))))
      (call-next-method)))

(defmethod convert ((to (eql 'Data)) (table hash-table) &key)
  (convert-from-hash-table 'Data table))

(defmethod convert :around ((to (eql 'hash-table)) (src Data) &key)
  (let ((table (call-next-method)))
    (dict* table "interface" (string (class-name-of src)))))

(define-interface InferredType (Data)
  (|name| :type string))

(define-interface TypeCandidate (Data)
  (|typeName| :type string)
  (|probability| :type number))

(define-interface TypeCandidates (Data)
  (|candidates| :type (proper-list TypeCandidate)))

(define-interface FunctionReturnType (Data)
  (|functionName| :type string
                  :optional t
                  :documentation "The name of the function (if it has one).")
  (|returns| :type string
             :documentation "The type the function returns."))

(define-interface FunctionReturnTypeCandidate (FunctionReturnType)
  (|probability| :type number))

(define-interface FunctionReturnTypeCandidates (Data)
  (|candidates| :type (soft-list-of FunctionReturnTypeCandidate)))

(define-interface Tests (Data)
  (|tests| :type (proper-list Testable)))

;;; NB The terminology used here is per the ISTQB glossary at
;;; https://glossary.istqb.org/, except that we distinguish "results"
;;; (from the point of view of a test runner, the expected outcome)
;;; from "statuses" (from the POV of a test harness, whether the test
;;; succeeded or failed.

(deftype TestPath ()
  '(proper-list string))

;;; TEST RESULTS

(define-interface TestCaseResult (Data)
  (|path| :type (proper-list string) :optional t))

(define-interface UnitTestCaseResult (TestCaseResult)
  (|expected| :type (or string number boolean)))

(define-interface BinaryTestCaseResult (TestCaseResult)
  (|stdout| :type (or string null) :optional t)
  (|stderr| :type (or string null) :optional t)
  (|exitCode| :type number :documentation "A non-negative integer representing an exit code."))

;;; TEST STATUSES

(define-namespaced-constants TestStatusKind ()
  (Skipped 0)
  (Passed 1)
  (Failed 2)
  (Errored 3)
  (Unknown 7))

(define-interface TestStatus ()
  (testStatusKind :type TestStatusKind)
  (|path| :type (proper-list string))
  (|time| :type number :optional t
          :documentation "How long the test took to run.")
  (|message| :type string :optional t)
  (|trace| :type string :optional t))

;;; TESTABLES

(define-interface Testable (Data)
  (|path| :type (proper-list string))
  (|sourceText| :type string :optional t)
  (|disabled| :type boolean :optional t :initform nil))

(defmethod (setf test-path) ((value list) (self Testable))
  (setf (slot-value self '|path|) value))

;;; NB Not an after method!
(defmethod initialize-instance :around ((self Testable) &rest args
                                        &key name)
  "Allow subclasses of Testable to take a `:name' initarg."
  (multiple-value-call #'call-next-method
    self
    (values-list (remove-from-plist args :name))
    (if name
        (values :path (list name))
        (values))))

(defgeneric name (thing)
  (:method ((test Testable))
    (test-name test)))

(defgeneric test-name (test)
  (:method ((self Testable))
    (lastcar (slot-value self '|path|))))

(define-namespaced-constants TestConditionKind ()
  (Fatal 0)
  (Nonfatal 1))

(define-interface Example ()
  (|comparator| :type string :optional t
                :documentation "How the expected output should be compared to the actual output. ")
  (|callee| :type string)
  (|subscripts| :type (proper-list t)
                :optional t
                :initform '()
                :documentation
                "A list of subscripts to retrieve the final value.")
  (|arguments| :type (proper-list t)
               :documentation "The literal arguments to the callee.")
  (|output| :type t :documentation "What the callee is expected to return."))

(defgeneric arguments (test)
  (:method ((test Example))
    (slot-value test '|arguments|)))

(defmethod print-object ((self Example) stream)
  (if (or *print-readably* *print-escape*)
      (print-unreadable-object (self stream :type t)
        (format stream "~a" self))
      (with-slots (|callee| |arguments| |subscripts| |comparator| |output|)
          self
        (format stream "~a(~{~a~^,~})~@[[~{~a~^, ~}~]] ~a ~a"
                |callee| |arguments| |subscripts| |comparator| |output|)))
  self)

(define-interface TestCondition (Testable)
  (testConditionKind :type TestConditionKind)
  (|examples| :type (proper-list Example) :initform nil))

(define-namespaced-constants TestCaseKind ()
  (UnitTest 1)
  (BinaryTest 2))

(define-interface TestCase (Testable)
  (|target| :type (or string Location) :optional t)
  (testCaseKind :type TestCaseKind :optional t)
  (|conditions| :type (proper-list TestCondition) :initform nil)
  (|arguments| :type (proper-list (or string number))
               :optional t)
  (|expected| :type TestCaseResult :optional t))

(defmethod arguments ((test-case TestCase))
  (slot-value test-case '|arguments|))

(define-interface TestSuite (Testable)
  (|cases| :type (proper-list TestCase) :initform nil)
  (|verificationResults| :type (proper-list TestCaseResult)
                         :optional t
                         :initform '())
  ;; What is this for?
  (|environment| :type t :optional t)
  (|nestable| :type boolean :optional t :initform t))

(define-interface TestFramework (Data)
  (|name| :type string))

(define-interface gotoOptions ()
  (|uri| :type |DocumentUri|)
  (|position| :type |Position|))

;;; Forward Client Request
(define-interface ForwardedClientRequest ()
  (|methodRequest| :type string)
  ;; Create a massive argument interface with a ton of optionals for each thing.
  (|clientRequestParams| :type |ClientRequestParams|))

(define-interface ClientRequestParams ()
  (|definitionParams| :type |DefinitionParams| :optional t)
  (|referenceParams| :type |ReferenceParams| :optional t)
  (|documentHighlightParams| :type |DocumentHighlightParams| :optional t))


;;; Lispier methods and subclasses.

(defgeneric test-path (test)
  (:documentation "Path to a test, where the last element is the name
  of the test and the preceding elements are the names of its
  enclosing test suites.")
  (:method ((test Testable))
    (slot-value test '|path|)))

(defgeneric test-disabled-p (test)
  (:method ((test Testable))
    (slot-value test '|disabled|)))

(defgeneric test-file (test)
  (:method ((test TestCase))
    (match (test-target test)
      ((Location :uri (and uri (type string)))
       (drop-prefix "file://" uri)))))

(defgeneric test-target (test)
  (:method ((test TestCase))
    (slot-value test '|target|))
  (:method ((test TestCondition))
    (slot-value test '|target|))
  (:method ((test Example))
    (slot-value test '|target|)))

(defclass unit-test-case-result (UnitTestCaseResult)
  ((|expected| :reader output :initarg :output)))

(defmethod print-object ((obj unit-test-case-result) stream)
  (if *print-readably*
      (call-next-method)
      (print-unreadable-object (obj stream :type t)
        (format stream "result: ~S" (output obj)))))

(defclass binary-test-case-result (BinaryTestCaseResult)
  ((|stdout| :reader stdout)
   (|stderr| :reader stderr)
   (|exitCode| :reader exit-code)))

(defmethod print-object ((obj binary-test-case-result) stream)
  (if *print-readably*
      (call-next-method)
      (print-unreadable-object (obj stream :type t)
        (format stream "~s ~s ~d" (stdout obj) (stderr obj) (exit-code obj)))))

(deftype test-case-result ()
  '(or unit-test-case-result binary-test-case-result))

(defclass basic-test-status (TestStatus)
  ((|path| :reader test-path)
   (|message| :reader test-status-message))
  (:default-initargs :time 0))

(defmethod print-object ((self basic-test-status) stream)
  (print-unreadable-object (self stream :type t)
    (format stream "~{~a~^.~}" (test-path self))))

(defclass passed (basic-test-status)
  ()
  (:default-initargs :test-status-kind TestStatusKind.Passed))

(defclass skipped (basic-test-status)
  ()
  (:default-initargs :test-status-kind TestStatusKind.Skipped))

(defclass unknown (basic-test-status)
  ()
  (:default-initargs :test-status-kind TestStatusKind.Unknown))

(defclass basic-failure (basic-test-status)
  ())

(defmethod print-object ((self basic-failure) stream)
  (print-unreadable-object (self stream :type t)
    (format stream "~{~a~^.~} ~a"
            (test-path self)
            (test-status-message self))))

(defclass failed (basic-failure)
  ()
  (:default-initargs :test-status-kind TestStatusKind.Failed))

(defclass errored (basic-failure)
  ()
  (:default-initargs :test-status-kind TestStatusKind.Errored)
  (:documentation "Failure due to an unhandled error."))

(deftype test-status ()
  '(or passed failed errored skipped unknown))

(defclass example-call (Example)
  ((|comparator| :initarg :operator :reader comparator)
   (|callee| :initarg :name :reader name)
   (|arguments| :reader arguments)
   (|output| :reader output)
   (|subscripts| :reader subscripts)))

(defclass basic-test-condition (TestCondition)
  ((|examples| :reader test-condition-examples)))

(defclass fatal-test-condition (basic-test-condition)
  ()
  (:default-initargs :test-condition-kind TestConditionKind.Fatal))

(defclass non-fatal-test-condition (basic-test-condition)
  ()
  (:default-initargs :test-condition-kind TestConditionKind.Nonfatal))

(deftype test-condition ()
  '(or fata-test-condition non-fatal-test-condition))

(defgeneric test-case-conditions (test)
  (:method ((test TestCase))
    (slot-value test '|conditions|)))

(defgeneric test-case-examples (test)
  (:method (test)
    (mappend #'test-condition-examples
             (test-case-conditions test))))

(defgeneric test-condition-examples (test)
  (:method ((test TestCondition))
    (slot-value test '|examples|)))

(defclass basic-test-case (TestCase)
  ((|conditions| :reader test-case-conditions)
   (|target| :reader target)
   (|arguments| :reader arguments :initform nil)
   (|expected| :reader expected)))

(defmethod print-object ((self basic-test-case) stream)
  (print-unreadable-object (self stream :type t)
    (format stream "~{~a~^.~}" (test-path self))))

(defclass unit-test-case (basic-test-case)
  ((testCaseKind :initform TestCaseKind.UnitTest)))

;;; TODO Argot
(defclass fixture-unit-test-case (unit-test-case)
  ((|path| :accessor test-path)
   (setup :type (proper-list string) :initarg :setup :reader setup)
   (teardown :type (proper-list string) :initarg :teardown :reader teardown))
  (:default-initargs
   :setup nil
   :teardown nil))

(defclass binary-test-case (basic-test-case)
  ((testCaseKind :initform TestCaseKind.BinaryTest)))

(deftype test-case ()
  '(or unit-test-case binary-test-case))

;;; TODO Argot?
(defclass test-case-verification-result ()
  ((test-case :type test-case :initarg :test-case :reader test-case)
   (result :type test-case-result :initarg :result :reader result)
   (successful-p :type boolean :initarg :successful-p :reader successful-p))
  (:documentation "Encodes the RESULT of verifying a TEST CASE, including if it
was SUCCESSFUL-P.")
  (:default-initargs
   :result nil))

(defmethod print-object ((obj test-case-verification-result) stream)
  (if *print-readably*
      (call-next-method)
      (print-unreadable-object (obj stream :type t)
        (format stream "~s ~s ~s ~s"
                (name (test-case obj))
                (expected (test-case obj))
                (result obj)
                (successful-p obj)))))

(defclass test-suite (TestSuite)
  ((|cases| :reader test-suite-cases :accessor test-cases :initarg :test-cases)
   (|verificationResults| :accessor verification-results)
   (|environment| :reader environment)
   (|nestable| :reader nestable?))
  (:default-initargs :name "Anonymous test suite"))

(defmethod print-object ((self test-suite) stream)
  (print-unreadable-object (self stream :type t)
    (format stream "~{~a~^.~} (~a)"
            (test-path self)
            (length (test-suite-cases self)))))

(defclass test-framework (TestFramework)
  ((|name| :reader name)
   (test-framework-case-class
    :initarg :test-framework-case-class
    :reader test-framework-case-class
    :type (or symbol class))
   (test-framework-suite-class
    :initarg :test-framework-suite-class
    :reader test-framework-suite-class
    :type (or symbol class)))
  (:default-initargs
   :test-framework-case-class 'test-case
   :test-framework-suite-class 'test-suite))

(defmethod print-object ((self test-framework) stream)
  (print-unreadable-object (self stream :type t)
    (format stream "~a" (name self))))
