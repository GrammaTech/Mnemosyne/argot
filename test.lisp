(defpackage :argot/test
  (:use :gt/full :stefil+ :argot)
  (:export :test :batch-test :run-batch))
(in-package :argot/test)

(defroot test)
(defsuite argot "Argot tests")

(deftest test-interface-slot ()
  (is (typep (convert '|Data| (dict "name" "int" "interface" "InferredType"))
             '|InferredType|))
  (is (equal "InferredType"
             (gethash "interface"
                      (convert 'hash-table
                               (make '|InferredType| :name "int"))))))

(deftest test-interface-slot-set-on-creation ()
  (is (equal "Data" (slot-value (make '|Data|) '|interface|))))

(deftest test-contribution-data ()
  (let* ((contrib
           (convert '|Contribution|
                    (dict "source" "Unknown"
                          "kind" "types"
                          "data" (dict "interface" "TypeCandidates"
                                       "candidates"
                                       (list
                                        (dict "typeName" "int"
                                              "probability" 1))))))
         (data (slot-value contrib '|data|)))
    (is (typep data '|TypeCandidates|))
    (let ((candidates (slot-value data '|candidates|)))
      (is (every (of-type '|TypeCandidate|) candidates)))))

(deftest test-case-arguments ()
  (is (equal '(1 2 3)
             (arguments
              (make '|TestCase| :arguments '(1 2 3)
                    :path '())))))
